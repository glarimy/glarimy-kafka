package com.glarimy.kafka;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.IntegerDeserializer;

public class ResponseListener implements Runnable {
	private Consumer<Integer, LeaveResponse> consumer;

	public ResponseListener() {
		Properties props = new Properties();
		props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class.getName());
		props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, LeaveResponseDeserializer.class.getName());
		props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "employees");
		props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");

		consumer = new KafkaConsumer<Integer, LeaveResponse>(props);
		consumer.subscribe(Arrays.asList("leave-responses"));
	}

	@Override
	public void run() {
		while (true) {
			ConsumerRecords<Integer, LeaveResponse> records = consumer.poll(Duration.ofMillis(1000));
			for (ConsumerRecord<Integer, LeaveResponse> record : records) {
				if (record.value().isStatus()) {
					System.out.println("Leave Request: " + record.value().getLid() + " is approved");
				} else {
					System.out.println("Leave Request: " + record.value().getLid() + " is rejected");
				}
			}
		}
	}
}
