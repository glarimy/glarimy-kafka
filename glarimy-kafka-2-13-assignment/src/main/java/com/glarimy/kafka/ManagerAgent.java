package com.glarimy.kafka;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.IntegerSerializer;

public class ManagerAgent {
	public static void main(String[] args) throws Exception {
		ManagerAgent agent = new ManagerAgent();
		agent.processRequests();
	}

	@SuppressWarnings("resource")
	public void processRequests() throws Exception {
		Consumer<Integer, LeaveRequest> consumer;

		Properties props = new Properties();
		props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class.getName());
		props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, LeaveRequestDeserializer.class.getName());
		props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "manager");
		props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
		props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");

		consumer = new KafkaConsumer<Integer, LeaveRequest>(props);
		consumer.subscribe(Arrays.asList("leave-requests"));

		while (true) {
			ConsumerRecords<Integer, LeaveRequest> records = consumer.poll(Duration.ofMillis(1000));
			for (ConsumerRecord<Integer, LeaveRequest> record : records) {
				LeaveRequest request = record.value();
				System.out.println("Recieved leave request with ID: " + request.getLid());
				Thread.sleep(20000);
				LeaveResponse response = new LeaveResponse();
				response.setLid(request.getLid());
				response.setEid(request.getEid());
				response.setFrom(request.getFrom());
				response.setTo(request.getTo());
				response.setStatus(false);
				notifyEmployee(response);
				consumer.commitSync();
			}
		}
	}

	public void notifyEmployee(LeaveResponse response) throws Exception {
		Properties props = new Properties();
		props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
		props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, LeaveResponseSerializer.class.getName());
		props.setProperty(ProducerConfig.ACKS_CONFIG, "all");

		Producer<Integer, LeaveResponse> producer = new KafkaProducer<Integer, LeaveResponse>(props);

		ProducerRecord<Integer, LeaveResponse> record = new ProducerRecord<Integer, LeaveResponse>("leave-responses",
				response.getEid(), response);
		producer.send(record);
		producer.flush();
		producer.close();
		System.out.println("Responded for leave request with ID: " + response.getLid());
	}
}
