package com.glarimy.kafka;

import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;

public class EmployeeAgent {
	public static void main(String[] args) throws Exception {
		EmployeeAgent agent = new EmployeeAgent();
		agent.listenForResponses();
		agent.applyForLeaves();
	}

	public void applyForLeaves() throws Exception {
		Properties props = new Properties();
		props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
		props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, LeaveRequestSerializer.class.getName());
		props.setProperty(ProducerConfig.ACKS_CONFIG, "all");

		Producer<Integer, LeaveRequest> producer = new KafkaProducer<Integer, LeaveRequest>(props);
		int count = 0;
		while (count < 10) {
			LeaveRequest request = new LeaveRequest();
			request.setEid(1234);
			request.setFrom(new Date());
			request.setTo(new Date());

			ProducerRecord<Integer, LeaveRequest> record = new ProducerRecord<Integer, LeaveRequest>("leave-requests",
					request.getEid(), request);
			producer.send(record);
			producer.flush();
			System.out.println("Applied for leave with ID: " + request.getLid());
			count++;
			Thread.sleep(10000);
		}
		producer.close();
	}

	public void listenForResponses() throws Exception {
		new Thread(new ResponseListener()).start();
	}

}
