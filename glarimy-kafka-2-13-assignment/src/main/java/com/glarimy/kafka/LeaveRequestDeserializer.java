package com.glarimy.kafka;

import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class LeaveRequestDeserializer implements Deserializer<LeaveRequest> {
	@Override
	public LeaveRequest deserialize(String topic, byte[] data) {
		try {
			return new ObjectMapper().readValue(data, LeaveRequest.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
