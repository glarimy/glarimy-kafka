package com.glarimy.kafka;

import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class LeaveResponseDeserializer implements Deserializer<LeaveResponse> {
	@Override
	public LeaveResponse deserialize(String topic, byte[] data) {
		try {
			return new ObjectMapper().readValue(data, LeaveResponse.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
