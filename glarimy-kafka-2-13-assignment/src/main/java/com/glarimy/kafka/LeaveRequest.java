package com.glarimy.kafka;

import java.util.Date;
import java.util.Random;

public class LeaveRequest {
	private int lid;
	private int eid;
	private Date from;
	private Date to;

	public LeaveRequest() {
		this.lid = new Random().nextInt(1000);
	}

	public int getLid() {
		return lid;
	}

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}
}