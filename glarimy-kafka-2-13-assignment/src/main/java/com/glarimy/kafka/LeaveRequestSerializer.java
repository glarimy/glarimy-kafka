package com.glarimy.kafka;

import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LeaveRequestSerializer implements Serializer<LeaveRequest> {

	@Override
	public byte[] serialize(String topic, LeaveRequest req) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsBytes(req);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}

}
