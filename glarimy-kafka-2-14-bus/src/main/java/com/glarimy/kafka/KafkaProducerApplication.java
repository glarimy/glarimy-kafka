package com.glarimy.kafka;

import java.util.Properties;
import java.util.Random;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafkaProducerApplication {

	public static void main(String[] args) throws Exception {
		Properties props = new Properties();
		props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
		props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		Producer<Integer, String> producer = new KafkaProducer<Integer, String>(props);

		Random random = new Random();
		int count = 0;
		while (count++ < 50) {
			int trip = random.nextInt(3	);
			ProducerRecord<Integer, String> record = new ProducerRecord<Integer, String>("location", trip,
					"location update from " + trip);
			RecordMetadata rmd = producer.send(record).get();
			System.out.println("location update from " + trip + " is stored in partition: " + rmd.partition()
					+ " at offset " + rmd.offset());
		}
		producer.flush();
		producer.close();
	}

}
