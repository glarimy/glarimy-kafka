1. Install Spark
Download from https://www.apache.org/dyn/closer.lua/spark/spark-2.4.4/spark-2.4.4-bin-hadoop2.7.tgz
Extract the tarball

2. Develop Kafka-Spark application

3. Run Zookeeper and Kafka

4. Submit job to Spark
./spark-submit --class com.glarimy.kafka.WordProcessor path/to/target/your.jar
 
5. Verify the results