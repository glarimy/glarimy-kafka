package com.glarimy.kafka;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;

public class WordProcessor {

	public static void main(String[] args) throws Exception {

		SparkConf conf = new SparkConf().setAppName("sparkkafka");
		conf.setMaster("local[*]");
		JavaSparkContext sc = new JavaSparkContext(conf);
		JavaStreamingContext jsc = new JavaStreamingContext(sc, new Duration(10000));

		Collection<String> topics = Arrays.asList("spark-topic-1");

		HashMap<String, Object> kafkaprops = new HashMap<>();
		kafkaprops.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		kafkaprops.put(ConsumerConfig.GROUP_ID_CONFIG, "glarimy-group");
		kafkaprops.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class.getName());
		kafkaprops.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

		JavaInputDStream<ConsumerRecord<Integer, String>> ds = KafkaUtils.createDirectStream(jsc,
				LocationStrategies.PreferConsistent(),
				ConsumerStrategies.<Integer, String>Subscribe(topics, kafkaprops));
		
		JavaDStream<String> jds = ds.map(x -> x.value().toLowerCase());

		jds.foreachRDD(rdd -> {
			rdd.foreachPartition(partitionOfRecords -> {

				Producer<Integer, String> producer = SparkProducer.getProducer();
				while (partitionOfRecords.hasNext()) {
					producer.send(new ProducerRecord<>("spark-topic-2", 1, partitionOfRecords.next()));
				}

			});
		});

		jsc.start();
		jsc.awaitTermination();
	}
}
