package com.glarimy.kafka;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaConsumerApplication {

	public static void main(String[] args) {
		Logger logger = LoggerFactory.getLogger(KafkaConsumerApplication.class);
		CountDownLatch latch = new CountDownLatch(1);
		KafkaConsumerTask task = new KafkaConsumerTask(latch);
		Thread t = new Thread(task);
		t.start();
		try {
			logger.info("sleeping");
			Thread.sleep(60000);
			logger.info("shutting down the consumer");
			((KafkaConsumerTask) task).shutdown();
			latch.await();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
