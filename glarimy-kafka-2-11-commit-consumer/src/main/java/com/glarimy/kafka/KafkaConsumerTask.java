package com.glarimy.kafka;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaConsumerTask implements Runnable {
	private Logger logger = LoggerFactory.getLogger(KafkaConsumerTask.class);
	private Consumer<String, Transaction> consumer;
	private CountDownLatch latch;

	public KafkaConsumerTask(CountDownLatch latch) {
		this.latch = latch;

		Properties props = new Properties();
		props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "2-6-consumer");
		props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, TransactionDeserializer.class.getName());
		props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");

		consumer = new KafkaConsumer<String, Transaction>(props);
		consumer.subscribe(Arrays.asList("tx-topic"));
	}

	@Override
	public void run() {
		try {
			while (true) {
				ConsumerRecords<String, Transaction> records = consumer.poll(Duration.ofMillis(1000));
				for (ConsumerRecord<String, Transaction> record : records) {
					logger.info(record.toString());
					consumer.commitSync();
				}
			}
		} catch (WakeupException we) {
			logger.info("Stopped poll");
		} finally {
			consumer.close();
			latch.countDown();
		}
	}

	public void shutdown() {
		logger.info("Stopping poll");
		consumer.wakeup();
	}

}
