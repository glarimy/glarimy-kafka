package com.glarimy.kafka;

import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TransactionDeserializer implements Deserializer<Transaction> {
	@Override
	public Transaction deserialize(String topic, byte[] data) {
		try {
			return new ObjectMapper().readValue(data, Transaction.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
