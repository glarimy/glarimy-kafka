package com.glarimy.kafka;

import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class KafkaProducerApplication {

	// kafka-topics --alter --zookeeper localhost:2181 --topic pos --partitions 3

	public static void main(String[] args) throws Exception {
		Properties props = new Properties();
		props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, PrimaryKeySerializer.class.getName());
		props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, TransactionSerializer.class.getName());
		props.setProperty(ProducerConfig.PARTITIONER_CLASS_CONFIG, TransactionPartition.class.getName());
		Producer<PrimaryKey, Transaction> producer = new KafkaProducer<PrimaryKey, Transaction>(props);

		int pos = 9;

		while (pos > 0) {
			Transaction tx = new Transaction();
			tx.setDate(new Date());
			tx.setAmount(100);
			tx.setDescription("SKU 124");
			tx.setPos(pos--);
			ProducerRecord<PrimaryKey, Transaction> record = new ProducerRecord<PrimaryKey, Transaction>("tx-topic",
					new PrimaryKey(new Date(), tx.getPos()), tx);
			producer.send(record, new Callback() {

				@Override
				public void onCompletion(RecordMetadata metadata, Exception exception) {
					System.out.println("POS " + tx.getPos() + " logged to the partition: " + metadata.partition());
				}
			}).get();
		}

		while (pos < 9) {
			Transaction tx = new Transaction();
			tx.setDate(new Date());
			tx.setAmount(100);
			tx.setDescription("SKU 124");
			tx.setPos(pos++);
			ProducerRecord<PrimaryKey, Transaction> record = new ProducerRecord<PrimaryKey, Transaction>("tx-topic",
					new PrimaryKey(new Date(), tx.getPos()), tx);
			producer.send(record, new Callback() {

				@Override
				public void onCompletion(RecordMetadata metadata, Exception exception) {
					System.out.println("POS " + tx.getPos() + " logged to the partition: " + metadata.partition());
				}
			}).get();
		}

		producer.flush();
		producer.close();
	}

}
