package com.glarimy.kafka;

import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PrimaryKeySerializer implements Serializer<PrimaryKey> {

	@Override
	public byte[] serialize(String topic, PrimaryKey pk) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsBytes(pk);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}

}
