package com.glarimy.kafka;

import org.apache.kafka.clients.producer.internals.DefaultPartitioner;
import org.apache.kafka.common.Cluster;

public class TransactionPartition extends DefaultPartitioner {
	@Override
	public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
		PrimaryKey pk = (PrimaryKey) key;
		key = pk.getPos();
		keyBytes = Integer.toString(pk.getPos()).getBytes();
		return super.partition(topic, key, keyBytes, value, valueBytes, cluster);
	}
}
