package com.glarimy.kafka;

import java.util.Date;

public class PrimaryKey {
	private Date date;
	private int pos;

	public PrimaryKey() {

	}

	public PrimaryKey(Date date, int pos) {
		super();
		this.date = date;
		this.pos = pos;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	@Override
	public String toString() {
		return "PrimaryKey [date=" + date + ", pos=" + pos + "]";
	}

}
