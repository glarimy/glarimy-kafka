package com.glarimy.kafka;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaConsumerTask implements Runnable {
	private Logger logger = LoggerFactory.getLogger(KafkaConsumerTask.class);
	private Consumer<String, Transaction> consumer;
	private CountDownLatch latch;

	public KafkaConsumerTask(CountDownLatch latch) {
		this.latch = latch;

		Properties props = new Properties();
		props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, TransactionDeserializer.class.getName());
		props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

		consumer = new KafkaConsumer<String, Transaction>(props);
		TopicPartition partition = new TopicPartition("tx-topic", 0);
		consumer.assign(Arrays.asList(partition));
		long offset = 300L;
		consumer.seek(partition, offset);
	}

	@Override
	public void run() {
		try {
			while (true) {
				ConsumerRecords<String, Transaction> records = consumer.poll(Duration.ofMillis(1000));
				for (ConsumerRecord<String, Transaction> record : records) {
					logger.info(record.toString());
				}
			}
		} catch (WakeupException we) {
			logger.info("Stopped poll");
		} finally {
			consumer.close();
			latch.countDown();
		}
	}

	public void shutdown() {
		logger.info("Stopping poll");
		consumer.wakeup();
	}

}
