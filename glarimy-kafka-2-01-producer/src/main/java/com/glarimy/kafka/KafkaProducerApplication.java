package com.glarimy.kafka;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafkaProducerApplication {

	public static void main(String[] args) throws Exception {
		Properties props = new Properties();
		props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		System.out.println("Creating producer");
		
		
		Producer<String, String> producer = new KafkaProducer<String, String>(props);
		System.out.println("Created producer");

		ProducerRecord<String, String> record 
			= new ProducerRecord<String, String>("first-topic", "Hello World!");

		System.out.println("Sending message");
		System.out.println(producer.send(record).get().offset());
		System.out.println("Sent message");
		producer.flush();
		producer.close();
	}

}
