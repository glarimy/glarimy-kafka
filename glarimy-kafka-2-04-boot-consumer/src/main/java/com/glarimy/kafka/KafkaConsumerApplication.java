package com.glarimy.kafka;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
public class KafkaConsumerApplication implements CommandLineRunner {
	@KafkaListener(topics = "spring-topic", groupId = "2-4-consumer")
	public void onMessage(String message) {
		System.out.println(message);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(KafkaConsumerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	}
}
