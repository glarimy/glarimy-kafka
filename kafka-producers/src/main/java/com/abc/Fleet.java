package com.abc;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.IntStream;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class Fleet {

	public static void main(String[] args) throws Exception {
		String broker = "localhost:9092";
		Properties props = new Properties();
		props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, broker);
		props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

		Producer<String, String> producer = new KafkaProducer<String, String>(props);
		String topic = "abc-bus-location";
		Map<String, String> locations = new HashMap<>();
		locations.put("BLRHBL001", "13.071362, 77.461906");
		locations.put("BLRHBL002", "14.399654, 76.045834");
		locations.put("BLRHBL003", "15.183959, 75.137622");
		locations.put("BLRHBL004", "13.659576, 76.944675");
		locations.put("BLRHBL005", "12.981337, 77.596181");
		locations.put("BLRHBL006", "13.024843, 77.546983");

		IntStream.range(0, 10).forEach(i -> {
			for (String trip : locations.keySet()) {
				ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, trip,
						locations.get(trip));
				producer.send(record);
			}
		});
		producer.flush();
		producer.close();
	}
}