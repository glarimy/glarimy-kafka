package com.glarimy.kafka;

import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafkaProducerApplication {

	// ./bin/kafka-topics.sh --create --zookeeper localhost:2181 --topic pos-tx-topic --partitions 3 --replication-factor 1

	public static void main(String[] args) throws Exception {
		Properties props = new Properties();
		props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, TransactionSerializer.class.getName());
		Producer<String, Transaction> producer = new KafkaProducer<String, Transaction>(props);

		int pos = 0;

		while (pos < 9) {
			Transaction tx = new Transaction();
			tx.setDate(new Date());
			tx.setAmount(100);
			tx.setDescription("SKU 124");
			tx.setPos(pos++);
			ProducerRecord<String, Transaction> record = new ProducerRecord<String, Transaction>("pos-tx-topic", tx);
			producer.send(record, new Callback() {

				@Override
				public void onCompletion(RecordMetadata metadata, Exception exception) {
					System.out.println("POS " + tx.getPos() + " logged to the partition: " + metadata.partition());
				}
			}).get();
		}

		while (pos > 0) {
			Transaction tx = new Transaction();
			tx.setDate(new Date());
			tx.setAmount(100);
			tx.setDescription("SKU 124");
			tx.setPos(pos--);
			ProducerRecord<String, Transaction> record = new ProducerRecord<String, Transaction>("pos-tx-topic",
					Integer.toString(tx.getPos()) + "", tx);
			producer.send(record, new Callback() {

				@Override
				public void onCompletion(RecordMetadata metadata, Exception exception) {
					System.out.println("POS " + tx.getPos() + " logged to the partition: " + metadata.partition());
				}
			}).get();
		}

		while (pos < 9) {
			Transaction tx = new Transaction();
			tx.setDate(new Date());
			tx.setAmount(100);
			tx.setDescription("SKU 124");
			tx.setPos(pos++);
			ProducerRecord<String, Transaction> record = new ProducerRecord<String, Transaction>("pos-tx-topic",
					Integer.toString(tx.getPos()) + "", tx);
			producer.send(record, new Callback() {

				@Override
				public void onCompletion(RecordMetadata metadata, Exception exception) {
					System.out.println("POS " + tx.getPos() + " logged to the partition: " + metadata.partition());
				}
			}).get();
		}

		producer.flush();
		producer.close();
	}

}
