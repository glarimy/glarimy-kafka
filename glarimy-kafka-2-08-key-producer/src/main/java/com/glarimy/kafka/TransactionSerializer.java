package com.glarimy.kafka;

import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TransactionSerializer implements Serializer<Transaction> {

	@Override
	public byte[] serialize(String topic, Transaction tx) {
		/*
		 * byte[] dateBytes = tx.getDate().toString().getBytes(); byte[]
		 * descriptionBytes = tx.getDescription().getBytes();
		 * 
		 * ByteBuffer buffer = ByteBuffer.allocate(dateBytes.length +
		 * descriptionBytes.length + 4 + 4); buffer.put(dateBytes);
		 * buffer.put(descriptionBytes); buffer.putInt(tx.getAmount());
		 * buffer.putInt(tx.getPos());
		 * 
		 * return buffer.array();
		 */
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsBytes(tx);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}

}
