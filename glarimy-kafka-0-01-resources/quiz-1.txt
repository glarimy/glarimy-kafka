POST-TRAINING QUIZ on APACHE KAFKA

1. What is the role of zookeeper in Kafka cluster?
A) To enable leader election
B) To act as transport channel 
C) To act as data source
D) All the above

2. What is the minimum of number bootstrap brokers must present for Kafka to work?
A) None
B) One
C) Three
D) Five

3. What is true about reading messages from a topic-partition?
A) A given consumer receives the messages in the order in which they are logged in the topic-partition
B) Not more than one consumer receives the messages from a given topic-partition, simultaneously 
C) Both A & B
D) None of the above

4. The primary role of Flume is ________________.
A) to process messages for business needs
B) to aggregate data
C) to store messages for persistence
D) to transform message structure

5. Which of the following demands aknowledgement of successful message replication?
A) ack=-1
B) ack=0
C) ack=1
D) ack=all

6. What is true about logging messages with key in topic-partitions, when the number of partitions remain same.
A) Messages with same key always logged into the same topic-partition
B) Messages with same key may be logged into multiple topic-partitions depending on the load
C) Messages with same key are ordered across partitions
D) Both B & C

7. What happens if N number of consumers are part of a consumer group against M number of partitions, where N > M?
A) Multiple consumers read messages from some of the partitions
B) Consumer group gets discarded
C) N-M number of consumers remain inactive
D) N-M number of consumers will be discarded from the consumer group

8. What is true about replication in Kafka?
A) Leader pushes messages to the followers
B) Followers pull messages from the leader
C) Unclean leader may result in message loss
D) Both B & C

9. How many zookeeper instances must run to create a 5-broker kafka cluster on a single machine?
A) 1
B) 2
C) 3
D) 5

10. Which of the following is default serializer in Kafka?
A) StringSerializer
B) ByteArraySerializer
C) ObjectSerializer
D) No such default serializer

11. What is the value that represents absence of key against a message, while producing the message?
A) -1
B) null
C) 0
D) None of the above

12. Which of the following ensures better reliability when Spark reads messages from Kafka?
A) Receiver based approach
B) Sink Connector approach
C) Direct Approach
D) Source Connector approach

13. Which of the following delivery semantics may result in duplicate messages?
A) At-most-once
B) At-least-once
C) Exactly-once
D) As-many

14. What is the role of Flume with respect to Kafka?
A) Producer
B) Consumer
C) Broker
D) Both A & B

15. Which one of the following improves Kafka + Spark performance?
A) Increasing the number of partitions in Kafka
B) Reducing the number of partitions in Kafka
C) Increasing the number of replications in Kafka
D) Reducing the number of replications in Kafka

16. What are the possible reasons if Kafka producer reports 'Connection Refused'
A) Kafka broker might not be reachable
B) Kafka is in the middle of leader election
C) Kafka partitions are completely filled
D) Both A & B

17. Which of the following Flume channel is suitable for high-throughput?
A) SpillableMemoryChannel
B) FileChannel
C) MemoryChannel
D) The throughput is same across all channels

18. Which of the following is used for schema based JSON, in Flume?
A) JsonSource
B) AvroSource
C) SchemaSource
D) StringSource

19. What is the unique way to identify a message in Kafka?
A) Topic
B) Topic + Partition
C) Topic + Partition + Offset
D) Topic + Partition + Offset + Relication

20. Why do you choose Kafka over RabbitMQ?
A) Kafka has no limit on the number of consumers
B) Kafka has a limit on the number of consumers
C) Kafka works only with Java using JMS
D) Kafka works with any programming language with a client

KEY: 1-A, 2-B, 3-C, 4-B, 5-D, 6-A, 7-C, 8-B, 9-1, 10-D, 11-B, 12-D, 13-B, 14-D, 15-A, 16-D, 17-A, 18-B, 19-C, 20-A