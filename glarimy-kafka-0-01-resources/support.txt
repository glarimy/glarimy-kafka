1. Remove existing infrastructure (/tmp/kafka-logs, /tmp/zookeeper, kafka)

2. Extract kafka tar
tar -xvf <kafka.tgz>

3. Verify kafka home folder
cd <kafka-home>/

4. Start zookeeper
./bin/zookeeper-server-start.sh ./config/zookeeper.properties

5. Verify /tmp and ./logs/server.log

6. Check the controller
./bin/zookeeper-shell localhost:2181 get /controller

7. Start kafka
./bin/kafka-server-start.sh ./config/server.properties

8. Check the controller
./bin/zookeeper-shell localhost:2181 get /controller

