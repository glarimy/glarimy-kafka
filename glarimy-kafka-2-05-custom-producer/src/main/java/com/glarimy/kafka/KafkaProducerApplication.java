package com.glarimy.kafka;

import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafkaProducerApplication {

	public static void main(String[] args) {
		Properties props = new Properties();
		props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
		props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, TransactionSerializer.class.getName());
		
		System.out.println("Creating producer");
		Producer<String, Transaction> producer = new KafkaProducer<String, Transaction>(props);
		System.out.println("Created producer");

		Transaction tx = new Transaction();
		tx.setDate(new Date());
		tx.setAmount(100);
		tx.setDescription("SKU 123");
		tx.setPos(45678);
		ProducerRecord<String, Transaction> record 
		= new ProducerRecord<String, Transaction>("tx-topic", tx);

		System.out.println("Sending message");
		producer.send(record);
		System.out.println("Sent message");
		producer.flush();
		producer.close();
	}

}
