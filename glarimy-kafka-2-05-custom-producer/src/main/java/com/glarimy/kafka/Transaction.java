package com.glarimy.kafka;

import java.util.Date;

public class Transaction {
	private Date date;
	private String description;
	private int amount;
	private int pos;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getPos() {
		return pos;
	}
	public void setPos(int pos) {
		this.pos = pos;
	}
	@Override
	public String toString() {
		return "Transaction [date=" + date + ", description=" + description + ", amount=" + amount + ", pos=" + pos
				+ "]";
	}


}
