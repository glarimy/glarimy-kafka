Installing Kafka

1. Download Kafka 2.8.0 binary with Scala 2.12
2. `tar -xvf <kafka-archive>`
3. Move to the kafka home folder
4. Uncomment line 31 in the `./config/server.properties`

Running Kafka

1. `./bin/zookeeper-server-start.sh ./config/zookeeper.properties`
2. `./bin/kafka-server-start.sh ./config/server.properties`

Running Console Clients

1. `./bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning`
2. `./bin/kafka-console-producer.sh --bootstrap-server localhost:9092 --topic test`

Stopping Kafka

1. `./bin/kafka-server-stop.sh`
2. `./bin/zookeeper-server-stop.sh`

Building and running applications

0. Install Maven if it is not already done
1. Move to the project folder
2. `mvn clean package`
3. `java -cp ./target/<jar-with-dependencies> <main-class>` 