package com.labs;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

public class ProducerService {

	public static void main(String[] args) throws Exception {
		String topic = "abc.location.update";
		String trip = "BLRHBL003";
		String location = "13.071362,77.461906";
		String broker = "localhost:9092";
		ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, trip, location);

		Properties props = new Properties();
		props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, broker);
		props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

		Producer<String, String> producer = new KafkaProducer<String, String>(props);

		producer.send(record);
		producer.flush();
		producer.close();
	}

}